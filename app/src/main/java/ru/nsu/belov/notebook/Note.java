package ru.nsu.belov.notebook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Evgeny on 25.09.2017.
 */

public class Note {
    String title;
    String text;
    String date;
    private int id;

    public Note(String title) {
        this(title, new SimpleDateFormat("HH:mm:ss dd-MM-yyyy", Locale.US)
                .format(Calendar.getInstance().getTime()), "");
    }

    public Note(String title, String data, String text) {
        id = NoteBook.getNextId();
        this.title = title;
        this.date = data;
        this.text = text;
    }

    byte[] getArray() {
        byte[] textByteArray = text.getBytes(StandardCharsets.UTF_8);
        return ByteBuffer.allocate(4 + 19 + textByteArray.length)
                .putInt(textByteArray.length)
                .put(date.getBytes(StandardCharsets.UTF_8))
                .put(textByteArray).array();
    }



    @SuppressWarnings("ResultOfMethodCallIgnored")
    public boolean save(String text) {
        this.text = text;
        File newNoteFile = new File(NoteBook.getInstance().getApplicationContext().getFilesDir(),
                title + ".note");
        newNoteFile.delete();
        try {
            newNoteFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(newNoteFile);
            fileOutputStream.write(getArray());
            fileOutputStream.close();
            return true;


        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;
        return id == note.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }
}
