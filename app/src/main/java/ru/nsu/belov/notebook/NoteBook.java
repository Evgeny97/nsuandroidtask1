package ru.nsu.belov.notebook;

import android.app.Application;
import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Evgeny on 25.09.2017.
 */

public class NoteBook extends Application {
    private static NoteBook noteBookInstance = null;
    private static AtomicInteger atomicInteger = new AtomicInteger();
    private ArrayList<Note> notes;
    private File confFile = null;


    public static NoteBook getInstance() {
        if (noteBookInstance == null) {
            //noteBookInstance = new NoteBook();
            throw new RuntimeException("Note book not initialized");
        }
        return noteBookInstance;
    }

    public static int getNextId() {
        return atomicInteger.getAndIncrement();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //confSetup();
        notes = new ArrayList<>();
        for (File noteFile : getFilesDir().listFiles()) {
            if (noteFile.getName().endsWith(".note")) {
                try {
                    ByteBuffer buffer = ByteBuffer.allocate(4 + 19);
                    FileInputStream fileInputStream = new FileInputStream(noteFile);
                    fileInputStream.read(buffer.array());
                    int textSize = buffer.getInt();
                    String creationDate = new String(buffer.array(), 4, 19, StandardCharsets.UTF_8);
                    byte[] text = new byte[textSize];
                    fileInputStream.read(text);
                    fileInputStream.close();
                    String noteTitle = noteFile.getName().substring(0, noteFile.getName().length() - 5);
                    String noteText = new String(text,StandardCharsets.UTF_8);
                    notes.add(new Note(noteTitle, creationDate, noteText));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        noteBookInstance = this;
    }

    private void confSetup() {
        confFile = new File(getFilesDir(), "notebook.conf");
        try {
            if (!confFile.createNewFile()) {
                writeConfFile();
            } else {
                readConfFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    void writeConfFile() throws IOException {
        if (confFile != null) {
            FileOutputStream fileOutputStream = new FileOutputStream(confFile);
            fileOutputStream.write(ByteBuffer.allocate(4).putInt(atomicInteger.get()).array());
            fileOutputStream.close();
        }
    }

    void readConfFile() throws IOException {
        if (confFile != null) {
            FileInputStream fileInputStream = new FileInputStream(confFile);
            ByteBuffer byteBuffer = ByteBuffer.allocate(4);
            boolean fourBytesRead = (fileInputStream.read(byteBuffer.array()) == 4);
            fileInputStream.close();
            if (fourBytesRead) {
                atomicInteger.set(byteBuffer.getInt());
            } else {
                throw new IOException("Can't read 4 bytes from confFile");
            }
        }
    }


    public Note getNote(int id) {
        for (Note note : notes) {
            if (note.getId() == id) {
                return note;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public void addNote(String title) {
        Note newNote = new Note(title);
        notes.add(newNote);
        File newNoteFile = new File(getFilesDir(), newNote.getTitle() + ".note");
        try {
            if (newNoteFile.createNewFile()) {
                FileOutputStream fileOutputStream = new FileOutputStream(newNoteFile);
                fileOutputStream.write(newNote.getArray());
                fileOutputStream.close();

            } else {
                //TODO добавь обработку
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
