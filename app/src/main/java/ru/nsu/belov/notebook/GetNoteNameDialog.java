package ru.nsu.belov.notebook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by Evgeny on 21.10.2017.
 */

public class GetNoteNameDialog extends DialogFragment {


//    public interface OnButtonClickListener{
//        void onButtonClick(String str);
//    }
//    //OnButtonClickListener onButtonClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.get_note_name_dialog, null);
        v.findViewById(R.id.buttonOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).onTitleSet(((EditText)v.findViewById(R.id.dialog_edit_text))
                        .getText().toString());
            }
        });
        return v;
    }
}
