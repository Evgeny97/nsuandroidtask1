package ru.nsu.belov.notebook;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TextEditorActivity extends AppCompatActivity {

    public static final String NOTE_ID = "NOTE_ID";
    @BindView(R.id.my_toolbar)
    public Toolbar toolbar;
    @BindView(R.id.note_title)
    public TextView titleTextView;
    @BindView(R.id.note_text)
    public TextView textTextView;
    private Note note;

    public static Intent getStartIntent(AppCompatActivity sourceActivity, int noteId) {
        Intent intent = new Intent(sourceActivity, TextEditorActivity.class);
        intent.putExtra(NOTE_ID, noteId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_editor);
        ButterKnife.bind(this);
        note = NoteBook.getInstance().getNote(getIntent().getIntExtra(NOTE_ID, -1));
        titleTextView.setText(note.title);
        textTextView.setText(note.text);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.text_edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                note.save(textTextView.getText().toString());
//                Typeface roboto = Typeface.createFromAsset(this.getAssets(), "font/Roboto-Bold.ttf");
//                titleTextView.setTypeface(roboto);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
