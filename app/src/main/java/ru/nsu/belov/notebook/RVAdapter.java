package ru.nsu.belov.notebook;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Evgeny on 25.09.2017.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.NoteViewHolder> {
    MainActivity activity;

    public RVAdapter(MainActivity activity) {
        this.activity = activity;
    }

    @Override

    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        NoteViewHolder pvh = new NoteViewHolder(v, activity);
        return pvh;
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        ArrayList<Note> notes = NoteBook.getInstance().getNotes();
        holder.bind(notes.get(position));

    }

    @Override
    public int getItemCount() {
        return NoteBook.getInstance().getNotes().size();
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {

        MainActivity activity;
        //        @BindView(R.id.card_view)
//        CardView cardView;
        Note note;
        @BindView(R.id.note_icon)
        ImageView imageView;
        @BindView(R.id.note_title)
        TextView titleTextView;
        @BindView(R.id.note_date)
        TextView dateTextView;

        NoteViewHolder(View itemView, MainActivity activity) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.activity = activity;
        }


        void bind(Note note) {
            this.note = note;
            titleTextView.setText(note.title);
            dateTextView.setText(note.date);
        }

        @OnClick(R.id.card_view)
        void onCardClick() {
            Intent intent = TextEditorActivity.getStartIntent(activity, note.getId());
            Pair<View, String> titlePair = Pair.create((View) titleTextView, activity.getResources().getString(R.string.transition_name_note_title));
            ActivityOptionsCompat options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(activity,titlePair);
            activity.startActivity(intent, options.toBundle());
        }



//        @OnLongClick(R.id.card_view)
//        boolean onLongCardClick() {
//            Intent intent = TextEditorActivity.getStartIntent(activity, note.getId());
//            //Pair<View, String> titlePair = Pair.create((View) titleTextView, activity.getResources().getString(R.string.transition_name_note_title));
//            ActivityOptionsCompat options = ActivityOptionsCompat
//                    .makeSceneTransitionAnimation(activity,(View) titleTextView, activity.getResources().getString(R.string.transition_name_note_title));
//            activity.startActivity(intent, options.toBundle());
//            return true;
//
//        }
    }
}
