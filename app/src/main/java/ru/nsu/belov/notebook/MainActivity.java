package ru.nsu.belov.notebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;
    RVAdapter rvAdapter;
    GetNoteNameDialog noteNameDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        rvAdapter = new RVAdapter(this);
        recyclerView.setAdapter(rvAdapter);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                Note rmNote = NoteBook.getInstance().getNotes().remove(position);
                rvAdapter.notifyItemRemoved(position);
                if (rmNote != null) {
                    new File(getFilesDir(), rmNote.getTitle() + ".note").delete();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @OnClick(R.id.floatingActionButton)
    void onFloatingActionButtonClick() {
        noteNameDialog = new GetNoteNameDialog();
        noteNameDialog.show(getSupportFragmentManager(), "GET_TITLE_FRAGMENT");

    }


    public void onTitleSet(String str) {
        if (!str.isEmpty()) {
            NoteBook.getInstance().addNote(str);
            rvAdapter.notifyDataSetChanged();
            if(noteNameDialog!=null){
                noteNameDialog.dismiss();
            }
        }
    }
}
